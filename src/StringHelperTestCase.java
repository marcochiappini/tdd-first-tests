import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringHelperTestCase {

	private StringHelper instanceOfStringHelper;
	@Before
	public void setUp() throws Exception {
		instanceOfStringHelper = new StringHelper();
	}
	
	@Test
	
	public void testStringEmpty(){
		assertEquals(instanceOfStringHelper.removeA(""), "");
	}
	
	
	@Test
	public void testRemoveAin1PosInStringWithOtherChars(){
		assertEquals(instanceOfStringHelper.removeA("ABCD"),"BCD");
	}
	
	@Test
	public void testRemoveAin2PosInStringWithOtherChars(){
		assertEquals(instanceOfStringHelper.removeA("BACD"),"BCD");
	}
	@Test
	public void testDoNothingIfAisOverTheSecondPosition(){
		assertEquals(instanceOfStringHelper.removeA("BCAAAA"),"BCAAAA");
	}
	
	@Test
	public void testWithAafterAndBeforeTheScndPosition(){
		assertEquals(instanceOfStringHelper.removeA("ABAAA"), "BAAA");
	}
}
